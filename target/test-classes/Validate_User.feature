Feature: As a QE, I validate Pets CRUD endpoints
#  @tg
  Scenario: Validating the Pets CRUD endpoints
    Given Create a user with the following data and send a POST request to
      | id         | 3              |
      | userName   | John           |
      | firstName  | Alex           |
      | lastName   | Smith          |
      | email      | alex@gmail.com |
      | password   | 123            |
      | phone      | 234-5643       |
      | userStatus | 7              |
    And Validate that status code is 200
