Feature:As a QA, I validate the Swagger API CRUD operation

  @tg
  Scenario Outline: validate pet and user
    Given Create a pet with the following data and send a POST request
      | petId        | 18           |
      | categoryId   | <categoryID> |
      | categoryName | Wolf         |
      | petName      | Wolf         |
      | photoURL     | my pet URL   |
      | tagId        | 85           |
      | tagName      | Wolf         |
      | status       | available    |
    And Validate that status code is 200
    And I send a PUT request to with the following data:
      | petId        | 18           |
      | categoryId   | <categoryID> |
      | categoryName | Wolf         |
      | petName      | <petName>    |
      | photoURL     | my pet URL   |
      | tagId        | 85           |
      | tagName      | <tagName>    |
      | status       | <status>     |

    And Validate that status code is 200
    And I send a GET request to "/v2/pet/"
    And I send a GET request to "/v2/pet/"
    When I send a DELETE request to "/v2/pet/"

    Examples:
      | categoryID | petName | tagName | status    |
      | 85         | Wolfy   | Wolfy   | available |