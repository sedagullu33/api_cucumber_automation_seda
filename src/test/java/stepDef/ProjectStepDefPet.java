package stepDef;

import com.jayway.jsonpath.JsonPath;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import pojoCasses.pets.Category;
import pojoCasses.pets.CreatePet;
import pojoCasses.pets.Tags;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static utils.ConfigReader.*;

import java.util.Collections;
import java.util.Map;

public class ProjectStepDefPet {

    Category category;
    Tags tags;
    CreatePet createPet;
    Response response;
    int actualPetId;

    @Given("Create a pet with the following data and send a POST request")
    public void createAPetWithTheFollowingDataAndSendAPOSTRequest(Map<String, String> petPost) {

        category = Category.builder()
                .id(Integer.parseInt(petPost.get("categoryId")))
                .name(petPost.get("categoryName")).build();

        tags = Tags.builder()
                .id(Integer.parseInt(petPost.get("tagId")))
                .name(petPost.get("tagName")).build();

        createPet = CreatePet.builder()
                .id(Integer.parseInt(petPost.get("petId")))
                .name(petPost.get("petName"))
                .category(category)
                .photoUrls(Collections.singletonList(petPost.get("photoURL")))
                .tags(Collections.singletonList(tags))
                .status(petPost.get("status")).build();

        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .body(createPet)
                .post(getProperty("petBaseUrl") + "/v2/pet")
                .then().extract().response();

        actualPetId = JsonPath.read(response.asString(), "id");
    }

    @And("I send a PUT request to with the following data:")
    public void iSendAPUTRequestToWithTheFollowingData(Map<String, String> petPut) {
        category = Category.builder()
                .id(Integer.parseInt(petPut.get("categoryId")))
                .name(petPut.get("categoryName")).build();

        tags = Tags.builder()
                .id(Integer.parseInt(petPut.get("tagId")))
                .name(petPut.get("tagName")).build();

        createPet = CreatePet.builder()
                .id(Integer.parseInt(petPut.get("petId")))
                .name(petPut.get("petName"))
                .category(category)
                .photoUrls(Collections.singletonList(petPut.get("photoURL")))
                .tags(Collections.singletonList(tags))
                .status(petPut.get("status")).build();

        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .body(createPet)
                .post(getProperty("petBaseUrl") + "/v2/pet")
                .then().extract().response();

    }

    @And("Validate that status code is {int} in common step def")
    public void validateThatStatusCodeIsInCommonStepDef(int expectedStatusCode) {
        int actualStatusCode = response.getStatusCode();

        assertThat(
                "I am expecting status code: " + expectedStatusCode,
                actualStatusCode,
                is(expectedStatusCode)
        );
    }

    @And("I send a GET request to {string}")
    public void iSendAGETRequestTo(String getUrl) {

        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .get(getProperty("petBaseUrl") + getUrl + actualPetId)
                .then().extract().response();

    }


    @When("I send a DELETE request to {string}")
    public void iSendADELETERequestTo(String deleteUrl) {
        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .delete(getProperty("petBaseUrl") + deleteUrl + actualPetId)
                .then().extract().response();
    }
}
