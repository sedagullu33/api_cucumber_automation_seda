package stepDef;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import pojoClasses.users.PetUser;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static utils.ConfigReader.getProperty;

public class ProjectStepDefUser {

    Response response;

    @Given("Create a user with the following data and send a POST request to")
    public void createAUserWithTheFollowingDataAndSendAPOSTRequestTo(Map<String, String> userData) {
        PetUser petUser = PetUser.builder()
                .id(Integer.parseInt(userData.get("id")))
                .username(userData.get("userName"))
                .firstName(userData.get("firstName"))
                .lastName(userData.get("lastName"))
                .email(userData.get("email"))
                .password(userData.get("password"))
                .phone(userData.get("phone"))
                .userStatus(Integer.parseInt(userData.get("userStatus")))
                .build();


        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .body(petUser)
                .post(getProperty("petBaseUrl") + "/v2/pet")
                .then().extract().response();
    }

    @And("Validate that status code is {int}")
    public void validateThatStatusCodeIs(int expectedStatusCode) {
        int actualStatusCode = response.getStatusCode();


        assertThat(
                "I am expecting status code: " + expectedStatusCode,
                actualStatusCode,
                is(expectedStatusCode)
        );

    }
}
